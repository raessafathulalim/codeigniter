<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testing extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('test_model');
		$this->template->set_theme('default');
	}
	public function index()
	{
		$data['name'] = $this->test_model->getName();
		$this->template->build('test_view', $data);	
	}

}

/* End of file Testing.php */
/* Location: ./application/modules/testing/controllers/Testing.php */